import Layout from '../components/layout.js'
import Link from 'next/link'
import fetch from 'isomorphic-unfetch'
import {Media, Form, FormGroup, FormControl, Button} from 'react-bootstrap';

const cardStyle = {
    margin: "0px 20px",
    padding: "20px",
    width: "20rem"
  }

  const titleStyle = {
    margin: "10px 0px",
    padding: "20px",
    width: "20rem"
  }
 
const Index = (props) =>(
    <Layout>
        {props.weatherInfoArray.map((weatherInfo) => (
            <div class="main-card">
                <h2 style={titleStyle}>{weatherInfo.cityName}</h2>
                <div class="row">
                <div class="card" style={cardStyle}>
                        <img class="card-img-top" style={{height: "222px"}} src="../static/weather.svg" alt="Card image cap"/>
                        <div class="card-block">
                            <h4 class="card-title">Weather</h4>
                            {/* <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> */}
                        </div>
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item"><b>{weatherInfo.main}</b></li>
                            <li class="list-group-item"><b>{weatherInfo.description}</b></li>
                        </ul>
                        {/* <div class="card-block">
                            <p>aaa</p>
                        </div> */}
                    </div>
                    <div class="card" style={cardStyle}>
                        <img class="card-img-top" style={{height: "222px"}} src="../static/temperature.svg" alt="Card image cap"/>
                        <div class="card-block">
                            <h4 class="card-title">Temperature</h4>
                            {/* <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> */}
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><b>Current temperature: </b>{weatherInfo.temperatureSummary.currentTemperature}</li>
                            <li class="list-group-item"><b>Min temperature: </b>{weatherInfo.temperatureSummary.minTemperature}</li>
                            <li class="list-group-item"><b>Max: temperature: </b>{weatherInfo.temperatureSummary.minTemperature}</li>
                        </ul>
                    </div>
                    <div class="card" style={cardStyle}>
                        <img class="card-img-top" style={{height: "222px"}} src="../static/pressure.svg" alt="Card image cap"/>
                        <div class="card-block">
                            <h4 class="card-title">Additional Information</h4>
                            {/* <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> */}
                        </div>
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item"><b>Pressure:</b> {weatherInfo.temperatureSummary.pressure}hPa</li>
                            <li class="list-group-item"><b>Humidity</b>{weatherInfo.temperatureSummary.humidity}%</li>
                        </ul>
                    </div>
                </div>
            </div>
        ))}
    </Layout>
);

Index.getInitialProps = async function(){
    console.log("aaaaaa")
    const res = await fetch('http://localhost:8080/api/weather/ids/2950159,2634844')

    const data = await res.json()

    console.log(`Show data fetched. Count: ${data.length}`)

    return {
        weatherInfoArray :data
    }
}

export default Index
