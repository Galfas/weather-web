# This project is a weather information services.
 
This project is built in React with NextjS it consumes an api that abstract a weather information services.

It is built in react with NextJs because, SEO it is a important component today so having a isomorphic application can give us the SEO power with the SPA experience for the user.

## Running

### Dependency
You will need to run the java application in order to have available data to retrieve. 
See: https://bitbucket.org/Galfas/weather-app

### Docker 
 It is possible to launch the application as a container, in this case you need to have docker installed in your server,
  and then you just need to execute the next commands in the project's root:
  
 1. docker build -t weather-web:0.0.1 .
 2. docker run -p 3000:3000 weather-web:0.0.1


TO run both services in a container you will need to replace the services url on index.js for the ip of services api.

### Terminal 
This is a NodeJS applicationTo start the app in the terminal, execute:
```
npm run dev

```

## Next steps:
1. Insert Redux to handle changes
2. extract properties from the files
3. Create a docker compose to put the services api.
4. Create tests for the application